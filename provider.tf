provider "google" {
   credentials = "${file("./creds/serviceaccount.json")}"
   project     = "hieuai-demo" # REPLACE WITH YOUR PROJECT ID
   region      = "US"
 }
